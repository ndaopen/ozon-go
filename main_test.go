/*
Author: Nesterov Dmitrii
*/

package main

import "testing"

type TestPair struct {
	input []uint
	left  uint
	right uint
}

var Tests = []TestPair{
	{[]uint{}, 0, 0},
	{[]uint{1}, 0, 0},
	{[]uint{1, 2}, 0, 0},
	{[]uint{1, 2, 3}, 0, 0},
	{[]uint{1, 3, 2}, 1, 2},
	{[]uint{3, 2, 1}, 0, 2},
	{[]uint{1, 2, 5, 3, 6}, 2, 3},
	{[]uint{1, 2, 4, 5, 3, 9, 7, 6, 8, 10, 11}, 2, 8},
	{[]uint{1, 1, 1, 1, 1}, 0, 0},
	{[]uint{1, 1, 1, 1, 0}, 0, 4},
	{[]uint{2, 3, 4, 8, 1, 5, 6, 7}, 0, 7},
}

func TestFindUnsortedSubarray(t *testing.T) {
	for idx, data := range Tests {
		l, r := findUnsortedSubarray(data.input)
		t.Log(
			"For test #", idx+1, "with input", data.input,
			"expected (", data.left, "and", data.right, ")",
			"got (", l, "and", r, ")",
			", which is", l == data.left && r == data.right,
		)
		if l != data.left || r != data.right {
			t.Error(
				"--> test #", idx+1, "failed!",
			)
		}
	}
}
