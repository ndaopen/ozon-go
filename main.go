/*
Author: Nesterov Dmitrii
 */

package main

import "log"

/*
XVII Задача о поиске не отсортированного подмассива ⭐⭐

Дана коллекция частично отсортированных целых неотрицательных чисел
Нужно реализовать алгоритм поиска не отсортированного подмассива

func findUnsortedSubarray(array []uint) (left uint, right uint)
 */



/*
Several questions and refinements:

1. Is sort direction really unknown?
Note:	algorithm will work for ascending sort expected

2. What values expected in case array is empty or small?
Note:	algorithm will return left = 0 and right = 0 for empty arrays or with single item

3. What values expected in case array is fully sorted?
Note:	algorithm will return left = 0 and right = 0 for sorted arrays

4. What elements should be included into "unsorted subarray"?
All elements to make array fully sorted? or elements to remove to make array sorted?
Example: 1, 2, 4, 5, 3, 9, 7, 6, 8, 10, 11
4.1. To make elements sorted algorithm should return left = 2 (element 4) and right = 8 (element 8)
4.2. To remove unsorted subarray algorithm should return left = 5 (element 3) and right = 7 (element 7)
Note:	implemented algorithm will return all elements to make full array sorted (variant 4.1.)
*/

//
// Helper functions
//

func getFirstLeftUnsortedIdx(array []uint) (left uint) {
	for left = 0; left < uint(len(array)-1); left++ {
		if array[left] > array[left+1] {
			return left
		}
	}
	return uint(len(array))
}

func getFirstRightUnsortedIdx(array []uint) (right uint) {
	for right = uint(len(array) - 1); right > 0; right-- {
		if array[right-1] > array[right] {
			return right
		}
	}
	return 0
}

func getMinAndMaxValuesInRange(array []uint, left uint, right uint) (min uint, max uint) {
	min = array[left]
	max = array[left]
	for i := left + 1; i <= right; i++ {
		if min > array[i] {
			min = array[i]
		} else if max < array[i] {
			max = array[i]
		}
	}
	return
}

func getHigherThanMinAtLeftIdx(array []uint, min uint, stop uint) (idx uint) {
	idx = stop
	if idx == 0 {
		return
	}

	// note: i should be >= 0.
	// BUT i is uint here, so could not be negative.
	// Will check first element after for loop
	for i := stop - 1; i > 0; i-- {
		if array[i] > min {
			idx = i
		}
	}
	if array[0] > min {
		idx = 0
	}

	return
}

func getLowerThanMaxAtRightIdx(array []uint, max uint, start uint) (idx uint) {
	idx = start

	for i := idx + 1; i < uint(len(array)); i++ {
		if array[i] < max {
			idx = i
		}
	}

	return
}

//
// requested function
// algorithm complexity O(n), memory: O(1)
//

func findUnsortedSubarray(array []uint) (left uint, right uint) {
	if len(array) <= 1 {
		log.Println("empty or small array -> return 0,0")
		return 0, 0
	}

	left = getFirstLeftUnsortedIdx(array)
	if left == uint(len(array)) {
		log.Println("array sorted ascending -> return 0,0")
		return 0, 0
	}
	right = getFirstRightUnsortedIdx(array)

	min, max := getMinAndMaxValuesInRange(array, left, right)
	left = getHigherThanMinAtLeftIdx(array, min, left)
	right = getLowerThanMaxAtRightIdx(array, max, right)
	return
}
